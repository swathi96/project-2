/* Generated automatically. */
static const char configuration_arguments[] = "/home/rise/gitlab/riscv-tools/riscv-gnu-toolchain/build/../riscv-gcc/configure --target=riscv32-unknown-elf --prefix=/home/rise/tools/riscv32 --disable-shared --disable-threads --enable-languages=c,c++ --with-system-zlib --enable-tls --with-newlib --with-sysroot=/home/rise/tools/riscv32/riscv32-unknown-elf --with-native-system-header-dir=/include --disable-libmudflap --disable-libssp --disable-libquadmath --disable-libgomp --disable-nls --disable-tm-clone-registry --src=../../riscv-gcc --disable-multilib --with-abi=ilp32 --with-arch=rv32imac --with-tune=rocket 'CFLAGS_FOR_TARGET=-Os   -mcmodel=medlow' 'CXXFLAGS_FOR_TARGET=-Os   -mcmodel=medlow'";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "abi", "ilp32" }, { "arch", "rv32imac" }, { "tune", "rocket" } };
